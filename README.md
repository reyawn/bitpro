# BITPRO
Simple text BITmap PROcessor

This was an interesting challenge.  I broke it down into two parts:
1) The bitmap processing algorithm
2) Input / Output

I could see immediately that there were optimizations that could be performed to improve the algorithm, because the "worst case" implementation would be pretty terrible.  Worst case is an O(n^2) time where n=number of pixels.  As each pixel is tracked all of the sizes are checked again.

So a few optimizations:
1) It is possible to reduce the size of the "window" where the algorithm would run, during the run, because certain rows are complete already and won't be revisited.
2) With the above, it is possible to output rows before the entire input for a test is parsed.
3) There are various checks that can be performed to prevent running too far down a row.

## Setup
First, clone this this repo and install dependencies:

```
# git clone https://reyawn@bitbucket.org/reyawn/bitpro.git
# cd bitpro
# npm install
```

### Using

The script is designed to receive data from STDIN and output to STDOUT, so perfect for piping data:

**tests.txt**
```
2
4 4
0110
1001
1001
0110
3 2
111
010
101
```

Run the shell script:

```
# cat tests.txt | sh bitmap-processor.sh
```

Run the script with `typescript`:

```
# cat tests.txt | npm run start-ts
```

Or with pure JS for speed:

```
# cat tests.txt | npm start
```

You can also include the bitmap processor directly within your own JS/TS:

```
//-------------------------------------
// ES Modules
import BitmapProcessor from './bitpro';
// ...
//-------------------------------------
// Require
const BitmapProcessor = require('./bitpro').default;
// ...
```




### Development

Tests can be run with npm test:

```
# npm test
```

They are found in the `/test` directory, and are fully `typescript` enabled.
