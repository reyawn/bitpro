import BitmapProcessor from '../src/bitmap-processor';

// @todo needs an actual verification function

export const testProcessor = (processor: BitmapProcessor, input: string, expectedOutput: string) => {
  let actualOutput = '';
  input.split('\n').forEach((str) => process.nextTick(() => {
    processor.inputLine(str);
  }));

  processor.on('row', (row) => actualOutput += row + '\n');

  return new Promise((res, rej) => {
    processor.on('done', () => {
      try {
        expect(actualOutput).toEqual(expectedOutput);
        res();
      } catch (e) {
        rej(e);
      }
    });
    processor.on('error', rej);
  });
};

test('add the example output', async () => {
  const processor = new BitmapProcessor({
    rows: 3,
    cols: 4
  });
  const input = 
`0001
0011
0110`;
  const expectedOutput =
`3 2 1 0
2 1 0 0
1 0 0 1
`;

  await testProcessor(processor, input, expectedOutput);
});

test('add another, more complicated example for full coverage', async () => {
  const processor = new BitmapProcessor({
    rows: 12,
    cols: 12
  });
  const input =
`000001000000
100000000001
000000000000
000000000000
000000000000
000000000000
000000000000
000000000000
000000000000
000000000000
000000000000
000000000000`;
  const expectedOutput =
`1 2 3 2 1 0 1 2 3 3 2 1
0 1 2 3 2 1 2 3 3 2 1 0
1 2 3 4 3 2 3 4 4 3 2 1
2 3 4 5 4 3 4 5 5 4 3 2
3 4 5 6 5 4 5 6 6 5 4 3
4 5 6 7 6 5 6 7 7 6 5 4
5 6 7 8 7 6 7 8 8 7 6 5
6 7 8 9 8 7 8 9 9 8 7 6
7 8 9 10 9 8 9 10 10 9 8 7
8 9 10 11 10 9 10 11 11 10 9 8
9 10 11 12 11 10 11 12 12 11 10 9
10 11 12 13 12 11 12 13 13 12 11 10
`;

  await testProcessor(processor, input, expectedOutput);
});