"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = require("assert");
const readline = __importStar(require("readline"));
const awaiting_1 = require("awaiting");
const events_1 = require("events");
const bitmap_processor_1 = __importDefault(require("./bitmap-processor"));
exports.default = bitmap_processor_1.default;
function digits(num) {
    return Math.floor(Math.log10(num)) + 1;
}
class InputProcessor extends events_1.EventEmitter {
    constructor(options) {
        super();
        this.lineNum = 0;
        this.numTests = 0;
        this.testStartLine = 0;
        this.doneTests = 0;
        this.handleInputLine = (line) => __awaiter(this, void 0, void 0, function* () {
            try {
                if (this.lineNum === 0) {
                    this.numTests = this.parseNumTestsLine(line);
                }
                else if (!this.currentProcessor) {
                    const options = this.parseTestDescriptorLine(line);
                    // Set up a new processor for the test
                    this.currentProcessor = new bitmap_processor_1.default(options);
                    this.currentProcessor.on('line', this.handleOutputLine);
                    this.testStartLine = this.lineNum;
                }
                else {
                    this.currentProcessor.inputLine(line);
                    if (this.lineNum === this.testStartLine + this.currentProcessor.rows) {
                        // Clear the current processor
                        const currentProcessor = this.currentProcessor;
                        this.currentProcessor = undefined;
                        // Increment the completed tests counter
                        this.doneTests += 1;
                        // If all tests are done, close the readline input
                        if (this.numTests === this.doneTests) {
                            this.readLine.close();
                        }
                        // Wait for the current processor to be done so all
                        // output is written
                        yield awaiting_1.event(currentProcessor, 'done');
                        currentProcessor.off('line', this.handleOutputLine);
                        this.emit('done');
                    }
                }
                this.lineNum += 1;
            }
            catch (e) {
                this.handleClose(e);
            }
        });
        // @todo make sure the output stream is ready for writing
        this.handleOutputLine = (line) => {
            this.output.write(line + '\n');
        };
        this.handleClose = (err) => {
            if (err) {
                process.nextTick(() => this.emit('error', err));
            }
        };
        const { minCols = 1, maxCols = 182, minRows = 1, maxRows = 182, minTests = 1, maxTests = 1000, input, output } = options;
        this.minCols = minCols;
        this.maxCols = maxCols;
        this.minRows = minRows;
        this.maxRows = maxRows;
        const colsPart = `[0-9]{${digits(minCols)},${digits(maxCols)}}`;
        const rowsPart = `[0-9]{${digits(minRows)},${digits(maxRows)}}`;
        this.testDescriptorRegex = new RegExp(`^${rowsPart} ${colsPart}$`);
        this.minTests = minTests;
        this.maxTests = maxTests;
        this.numTestsRegex = new RegExp(`^[0-9]{${digits(minTests)},${digits(maxTests)}}$`);
        this.input = input;
        this.output = output;
        this.readLine = readline.createInterface({ input });
        this.readLine.on('line', this.handleInputLine);
        this.readLine.once('close', this.handleClose);
    }
    parseNumTestsLine(line) {
        assert_1.strict(this.numTestsRegex.test(line), 'Input error: invalid test count');
        const numTests = parseInt(line, 10);
        assert_1.strict(numTests >= this.minTests && numTests <= this.maxTests, 'Input error: invalid test count');
        return numTests;
    }
    parseTestDescriptorLine(line) {
        assert_1.strict(this.testDescriptorRegex.test(line), 'Input error: invalid test size');
        const [rows, cols] = line.split(' ').map((val) => parseInt(val, 10));
        assert_1.strict(rows >= this.minRows && rows <= this.maxRows, 'Input error: invalid row dimension');
        assert_1.strict(cols >= this.minCols && cols <= this.maxCols, 'Input error: invalid column dimension');
        return { rows, cols };
    }
}
function processInput(input, output) {
    const inputProcessor = new InputProcessor({
        input,
        output
    });
    inputProcessor.once('error', handleError);
    inputProcessor.once('done', () => process.exit());
}
function handleError(e) {
    console.error(e.message);
    process.exit(1);
}
if (require.main === module) {
    process.on('uncaughtException', handleError);
    processInput(process.stdin, process.stdout);
}
