"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = require("assert");
const events_1 = require("events");
var Color;
(function (Color) {
    Color[Color["Black"] = 0] = "Black";
    Color[Color["White"] = 1] = "White";
})(Color || (Color = {}));
class BitmapProcessor extends events_1.EventEmitter {
    constructor(options) {
        super();
        this.data = [];
        this._cols = options.cols;
        this._rows = options.rows;
    }
    get cols() {
        return this._cols;
    }
    get rows() {
        return this._rows;
    }
    inputLine(line) {
        const data = this.data;
        const rows = this._rows;
        assert_1.strict(data.length < rows, `Input error: received more rows than the expected ${rows}`);
        const cols = this._cols;
        assert_1.strict.equal(line.length, cols, `Input error: received a line longer than the expected ${cols}`);
        // Pre-filling each counter position with the
        // maximum column value is an optimization
        // that prevents additional edge case checks
        data.push(Array(cols).fill(cols + rows - 1));
        const i = data.length - 1;
        for (let j = 0; j < cols; j++) {
            // Parsing each character with base 2 allows more
            // easily filtering out bad input
            const color = parseInt(line[j], 2);
            this.insertPixel(i, j, color);
        }
        // @todo Can emit rows earlier for efficiency, still WIP
        if (data.length === rows) {
            for (const row of data) {
                process.nextTick(() => this.emit('line', row.join(' ')));
            }
            process.nextTick(() => {
                this.cleanup();
                this.emit('done');
            });
        }
    }
    cleanup() {
        this.data = [];
    }
    insertPixel(i, j, color) {
        assert_1.strict.notEqual(color, NaN, 'Input error: unexpected character');
        const data = this.data;
        let distance = 0;
        // White pixels update in both
        // up and left directions
        if (color === Color.White) {
            this.updateUp(i - 1, j, 1);
            this.updateLeft(i, j - 1, 1);
            // Black pixels only update themselves
            // based on their neighbors
        }
        else if (i > 0 && j > 0) {
            // Not on top or left edge
            distance = Math.min(data[i - 1][j], data[i][j - 1]) + 1;
        }
        else if (i > 0) {
            // Not on top edge
            distance = data[i - 1][j] + 1;
        }
        else if (j > 0) {
            // Not on left edge
            distance = data[i][j - 1] + 1;
        }
        else {
            return;
        }
        data[i][j] = distance;
    }
    updateUp(i, j, distance) {
        for (; i >= 0; --i) {
            let row = this.data[i];
            if (row[j] <= distance) {
                break;
            }
            row[j] = distance;
            ++distance;
            this.updateLeft(i, j - 1, distance);
            this.updateRight(i, j + 1, distance);
        }
    }
    updateLeft(i, j, distance) {
        const row = this.data[i];
        for (; j >= 0 && row[j] > distance; --j) {
            row[j] = distance;
            ++distance;
        }
    }
    updateRight(i, j, distance) {
        const row = this.data[i];
        for (; j < row.length && row[j] > distance; ++j) {
            row[j] = distance;
            ++distance;
        }
    }
}
exports.default = BitmapProcessor;
