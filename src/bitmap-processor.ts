import { strict as assert } from 'assert';
import { EventEmitter } from 'events';

enum Color {
  Black = 0,
  White = 1
}

interface IBitmapProcessorOptions {
  cols: number,
  rows: number
}

export default class BitmapProcessor extends EventEmitter {
  private _cols: number;
  private _rows: number;
  private data: number[][] = [];

  constructor(options: IBitmapProcessorOptions) {
    super();
    this._cols = options.cols;
    this._rows = options.rows;
  }

  get cols() {
    return this._cols;
  }

  get rows() {
    return this._rows;
  }

  inputLine(line: string) {
    const data = this.data;
    const rows = this._rows;
    assert(data.length < rows,
      `Input error: received more rows than the expected ${rows}`);

    const cols = this._cols;
    assert.equal(line.length, cols,
      `Input error: received a line longer than the expected ${cols}`);

    // Pre-filling each counter position with the
    // maximum column value is an optimization
    // that prevents additional edge case checks
    data.push(Array(cols).fill(cols + rows - 1));

    const i = data.length - 1;
    for (let j = 0; j < cols; j++) {
      // Parsing each character with base 2 allows more
      // easily filtering out bad input
      const color: Color = parseInt(line[j], 2);
      this.insertPixel(i, j, color);
    }

    // @todo Can emit rows earlier for efficiency, still WIP
    if (data.length === rows) {
      for (const row of data) {
        process.nextTick(() => this.emit('line', row.join(' ')));
      }
      process.nextTick(() => {
        this.cleanup();
        this.emit('done');
      });
    }
  }

  private cleanup() {
    this.data = [];
  }

  private insertPixel(i: number, j: number, color: Color) {
    assert.notEqual(color, NaN, 'Input error: unexpected character');
    const data = this.data;
    let distance: number = 0;

    // White pixels update in both
    // up and left directions
    if (color === Color.White) {
      this.updateUp(i - 1, j, 1);
      this.updateLeft(i, j - 1, 1);
    // Black pixels only update themselves
    // based on their neighbors
    } else if (i > 0 && j > 0) {
      // Not on top or left edge
      distance = Math.min(data[i - 1][j], data[i][j - 1]) + 1;
    } else if (i > 0) {
      // Not on top edge
      distance = data[i - 1][j] + 1;
    } else if (j > 0) {
      // Not on left edge
      distance = data[i][j - 1] + 1;
    } else {
      return;
    }

    data[i][j] = distance;
  }

  private updateUp(i: number, j: number, distance: number) {
    for (; i >= 0; --i) {
      let row = this.data[i];
      if (row[j] <= distance) {
        break;
      } 
      row[j] = distance;
      ++distance;
      this.updateLeft(i, j - 1, distance);
      this.updateRight(i, j + 1, distance);
    }
  }

  private updateLeft(i: number, j: number, distance: number) {
    const row = this.data[i];
    for (; j >= 0 && row[j] > distance; --j) {
      row[j] = distance;
      ++distance;
    }
  }

  private updateRight(i: number, j: number, distance: number) {
    const row = this.data[i];
    for (; j < row.length && row[j] > distance; ++j) {
      row[j] = distance;
      ++distance;
    }
  }
}