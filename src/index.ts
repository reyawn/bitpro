import { strict as assert } from 'assert';
import * as readline from 'readline';
import { event } from 'awaiting';
import { EventEmitter } from 'events';

import BitmapProcessor from './bitmap-processor';
export default BitmapProcessor;


function digits(num: number): number {
  return Math.floor(Math.log10(num)) + 1;
}

interface IInputProcessorOptions {
  minCols?: number,
  maxCols?: number,
  minRows?: number,
  maxRows?: number,
  minTests?: number,
  maxTests?: number,

  input: NodeJS.ReadableStream,
  output: NodeJS.WritableStream
}

class InputProcessor extends EventEmitter {
  private minCols: number;
  private maxCols: number;
  private minRows: number;
  private maxRows: number;
  private minTests: number;
  private maxTests: number;
  private input: NodeJS.ReadableStream;
  private output: NodeJS.WritableStream;

  private readLine: readline.Interface;
  private lineNum: number = 0;
  private numTests: number = 0;
  private numTestsRegex: RegExp;
  private testDescriptorRegex: RegExp;
  private testStartLine: number = 0;
  private doneTests: number = 0;
  private currentProcessor?: BitmapProcessor;

  constructor(options: IInputProcessorOptions) {
    super();
    const {
      minCols = 1,
      maxCols = 182,
      minRows = 1,
      maxRows = 182,
      minTests = 1,
      maxTests = 1000,
      input,
      output
    } = options;
    this.minCols = minCols;
    this.maxCols = maxCols;
    this.minRows = minRows;
    this.maxRows = maxRows;
    const colsPart = `[0-9]{${digits(minCols)},${digits(maxCols)}}`;
    const rowsPart = `[0-9]{${digits(minRows)},${digits(maxRows)}}`;
    this.testDescriptorRegex = new RegExp(`^${rowsPart} ${colsPart}$`);

    this.minTests = minTests;
    this.maxTests = maxTests;
    this.numTestsRegex = new RegExp(`^[0-9]{${digits(minTests)},${digits(maxTests)}}$`);

    this.input = input;
    this.output = output;

    this.readLine = readline.createInterface({ input });
    this.readLine.on('line', this.handleInputLine);
    this.readLine.once('close', this.handleClose);
  }

  private handleInputLine = async (line: string) => {
    try {
      if (this.lineNum === 0) {
        this.numTests = this.parseNumTestsLine(line);
      } else if (!this.currentProcessor) {
        const options = this.parseTestDescriptorLine(line);
  
        // Set up a new processor for the test
        this.currentProcessor = new BitmapProcessor(options);
        this.currentProcessor.on('line', this.handleOutputLine);
        this.testStartLine = this.lineNum;
      } else {
        this.currentProcessor.inputLine(line);
        if (this.lineNum === this.testStartLine + this.currentProcessor.rows) {
          // Clear the current processor
          const currentProcessor = this.currentProcessor;
          this.currentProcessor = undefined;
  
          // Increment the completed tests counter
          this.doneTests += 1;
  
          // If all tests are done, close the readline input
          if (this.numTests === this.doneTests) {
            this.readLine.close();
          }
  
          // Wait for the current processor to be done so all
          // output is written
          await event(currentProcessor, 'done');
          currentProcessor.off('line', this.handleOutputLine);
          this.emit('done');
        }
      }
      this.lineNum += 1;
    } catch (e) {
      this.handleClose(e);
    }
  }

  // @todo make sure the output stream is ready for writing
  private handleOutputLine = (line: string) => {
    this.output.write(line + '\n');
  }

  private handleClose = (err?: Error) => {
    if (err) {
      process.nextTick(() => this.emit('error', err));
    }
  }

  private parseNumTestsLine(line: string): number {
    assert(this.numTestsRegex.test(line), 'Input error: invalid test count');

    const numTests = parseInt(line, 10);
    assert(numTests >= this.minTests && numTests <= this.maxTests, 'Input error: invalid test count');

    return numTests;
  }

  private parseTestDescriptorLine(line: string): {rows: number, cols: number} {
    assert(this.testDescriptorRegex.test(line), 'Input error: invalid test size');

    const [rows, cols] = line.split(' ').map((val) => parseInt(val, 10));
    assert(rows >= this.minRows && rows <= this.maxRows, 'Input error: invalid row dimension');
    assert(cols >= this.minCols && cols <= this.maxCols, 'Input error: invalid column dimension');

    return {rows, cols};
  }
}

function processInput(input: NodeJS.ReadableStream, output: NodeJS.WritableStream) {
  const inputProcessor = new InputProcessor({
    input,
    output
  });
  inputProcessor.once('error', handleError);
  inputProcessor.once('done', () => process.exit());
}

function handleError(e: Error) {
  console.error(e.message);
  process.exit(1);
}

if (require.main === module) {
  process.on('uncaughtException', handleError);
  processInput(process.stdin, process.stdout);
}