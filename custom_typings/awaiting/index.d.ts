declare module "awaiting" {
  export function event(emitter: NodeJS.EventEmitter, eventName: string): Promise<[]>;
}
